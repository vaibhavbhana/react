import axios from "axios"
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const Home = () => {

    const [users, setUser] = useState([])


    useEffect(() => {
        loadUsers()
    }, []);
    const deleteuser = async id =>{
        await axios.delete('http://localhost:8001/users/'+id );
        loadUsers();
    };

    const loadUsers = async () => {
        const result = await axios.get(" http://localhost:8001/users");
        setUser(result.data);
        
    }
    
    return (
        <div className="container">
            <div>

                <h1>HOME PAGE</h1>

                <table class="table border shadow">
                    <thead class="table-dark">
                        <tr>
                            
                            <th scope="col">Username</th>
                            <th scope="col">Email</th>
                            <th scope="col">contact</th>
                            <th>Action</th>

                            
                        </tr>
                    </thead>
                    <tbody>
                        {users.map((user,index)=>(
                             <tr>
                             {/* <th scope ="row" > {index + 0}   </th> */}
                              <td>{user.username}</td>
                              <td>{user.email}</td>
                              <td>{user.contact}</td>
                              <td>
                                  <Link   to={`/user/${user.id}`}class="btn btn-primary">View</Link>
                                  <Link to={`/edit/${user.id}`} class="btn btn-outline-primary">Edit</Link>
                                  <Link class="btn btn-danger" onClick={()=> deleteuser(user.id)}>Delete</Link>
                              </td>
                         
                         </tr>

                        ))}
                       
                    </tbody>
                </table>
            </div>
        </div>

    )
}

export default Home