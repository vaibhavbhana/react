import { useState } from "react";
import { useHistory } from "react-router-dom";
const Contact =() => {


    const [username,setUsername]= useState('')
    const [email,setEmail]= useState('')
    const [password,setPassword]=useState('')
    const [contact,setContact]= useState('')
    const history = useHistory();
    const handleSubmit =(e)=>{
        e.preventDefault()
        const blog ={username,email,password,contact};
        fetch(' http://localhost:8001/users',{
            method: 'POST',
            headers: {"content-type":"application/json"},
            body:JSON.stringify(blog)
        }).then(() =>{
            console.log("new user  added")
            history.push('/')
        }
            
        );

    }


    return (
        <div className="container">
           <div className="contact">
               
               <h1 > User Registration form </h1>
               <form  onSubmit={handleSubmit}>   
               <div class="form-group">
                   
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control"  onChange={(e)=> setUsername(e.target.value)} placeholder="Enter Username" value={username} />
                    
                   
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email" class="form-control" onChange={(e)=> setEmail(e.target.value)} aria-describedby="emailHelp" placeholder="Enter email"  value={email}/>
                    
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contact</label>
                    <input type="text" class="form-control"  onChange={(e)=> setContact(e.target.value)} placeholder="Contact"  value={contact}/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control"  onChange={(e)=> setPassword(e.target.value)} placeholder="Password"  value={password}/>
                </div><br/>
               
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
           </div>
        </div> 

    )
}

export default Contact