import { useState ,useEffect} from "react";
import axios from "axios";
import { useParams } from "react-router-dom";

const User =()=>{
    const [user,setUser]=useState({
        username:"",
        email:"",
        contact:"",
        password:""

    });
    const {id} = useParams();
    const loaduser =async ()=>{
        const result =  await axios.get(" http://localhost:8001/users/"+ id);
        console.log(result);
        setUser(result.data)
     
 
    }
  useEffect(()=>{
      loaduser()
  },[])
    
    return(
<div className="container">
    <h1>username :{user.id }</h1>
    <ul className="list-group w-50">
    <li className=" list-group-item">name: {user.username}</li>
    <li className=" list-group-item">email: {user.email}</li>
    <li className=" list-group-item">contact: {user.contact}</li>
     <li className=" list-group-item">password: {user.password}</li>

    
    </ul>
</div>

     
    )
    }
    export default User