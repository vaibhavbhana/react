import { Link } from "react-router-dom"

const Navbar =()=>{
    return (
        <div classNameName="Navbar">
            <div classNameName="bhana">
            <nav className="navbar navbar-expand-lg navbar-primary bg-dark">
                <Link className="navbar-brand" to="#">React user</Link>

                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                    <li className="nav-item active">
                        <Link className="nav-link" to="/">Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/about">About us</Link>
                    </li>
                    <li className="nav-item">
                        <Link class="nav-link" to="/contact">Contact</Link>
                    </li>
                    <li className="nav-item">
                        <Link class="nav-link" to="/user/auth">Login</Link>
                    </li>
                
                    </ul>
                </div>
                </nav>
            </div>


        </div>
    )
}

export default Navbar





