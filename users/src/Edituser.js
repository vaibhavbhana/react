import axios from "axios";
import { useEffect, useState } from "react"
import { useHistory,useParams } from "react-router-dom";


const Edituser=()=>{
    // const [user,setuser]=useState({
    //     username:"",
    //     email:"",
    //     contact:""

    // });
    const [username,setUsername]= useState('')
    const [email,setEmail]= useState('')
    const [password,setPassword]=useState('')
    const [contact,setContact]= useState('')
    const history = useHistory();
    const handleSubmit =(e)=>{
        e.preventDefault()
        const blog ={username,email,password,contact};
        fetch(' http://localhost:8001/users/'+ id, {
            method: 'PUT',
            headers: {"content-type":"application/json"},
            body:JSON.stringify(blog)
        }).then(() =>{
            console.log("new user  added")
            history.push('/')
        }
            
        );

    }
    const {id} = useParams();
  
    const loaduser =async ()=>{
        const result =  await axios.get(" http://localhost:8001/users/"+ id);
        console.log(result);
        console.log(result.data.email)
        setUsername(result.data.username)
        setEmail(result.data.email)
        setContact(result.data.contact)
        setPassword(result.data.password)
 
    }
  useEffect(()=>{
      loaduser()
  },[]);


    return (
        <div className="container">
           <div>
               
               <h1> Edit USer</h1>
               <form  onSubmit={handleSubmit}>   
               <div class="form-group">
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" class="form-control"  onChange={(e)=> setUsername(e.target.value)} placeholder="Enter Username" value={username} />
                    
                   
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" onChange={(e)=> setEmail(e.target.value)} aria-describedby="emailHelp" placeholder="Enter email"  value={email}/>
                    
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Contact</label>
                    <input type="text" class="form-control"  onChange={(e)=> setContact(e.target.value)} placeholder="Contact"  value={contact}/>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control"  onChange={(e)=> setPassword(e.target.value)} placeholder="Password"  value={password}/>
                </div><br/>
               
                <button type="submit" class="btn btn-warning">Update User</button>
                
                </form>
           </div>
        </div> 

    )
}
export default Edituser