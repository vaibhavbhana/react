
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Home from './Home';
import { BrowserRouter as Router ,Route,Switch } from 'react-router-dom';
import Navbar from './Navbar'
import Contact from './Contact';
import About from './About';
import Edituser from './Edituser';
import User from './User';
import Auth from './Auth'
import SwaggerEditor, {plugins} from 'swagger-editor';
import 'swagger-editor/dist/swagger-editor.css';
import Login from './Login';


// import NotFound from './Notfound';



function App() {

// window.editor = SwaggerEditor({
//   dom_id: '#swagger-editor',
//   layout: 'EditorLayout',
//   plugins: Object.values(plugins),
//   swagger2GeneratorUrl: 'https://generator.swagger.io/api/swagger.json',
//   oas3GeneratorUrl: 'https://generator3.swagger.io/openapi.json',
//   swagger2ConverterUrl: 'https://converter.swagger.io/api/convert',
// });
  return (
    <Router>
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/contact"component={Contact} />
        <Route exact path="/about" component={About} /> 
        <Route exact path="/user/auth" component={Auth}/>
        <Route exact path="/edit/:id" component={Edituser}/>
        <Route  exact path = "/user/:id" component={User}/>
        
       
        {/* <Route component={NotFound} /> */}
      </Switch>
    </div>
    </Router>
  );

  
}


export default App;



// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// const [authLoading, setAuthLoading] = useState(true);
 
// useEffect(() => {
//   const token = getToken();
//   if (!token) {
//     return;
//   }

//   axios.get(`http://localhost:4000/verifyToken?token=${token}`).then(response => {
//     setUserSession(response.data.token, response.data.user);
//     setAuthLoading(false);
//   }).catch(error => {
//     removeUserSession();
//     setAuthLoading(false);
//   });
// }, []);

// if (authLoading && getToken()) {
//   return <div className="content">Checking Authentication...</div>
// }

// return (
//   <Router>
//   <div className="App">
    
//     <Navbar />
//         <Switch>
// //         <Route exact path="/" component={Home} />
// //         <Route exact path="/login" component={Login} />
// //         <Route exact path="/contact"component={Contact} />
// //         <Route exact path="/about" component={About} /> 
// //         <PublicRoute path="/user/auth" component={Auth}/>
// //         <Route exact path="/edit/:id" component={Edituser}/>
// //         <Route  exact path = "/user/:id" component={User}/>
// <Route path="/dashboard">
//             <Dashboard />
//           </Route>
//           <Route path="/preferences">
//             <Preferences /></Route>

        
       
// //         {/* <Route component={NotFound} /> */}
// //       </Switch>
       
//   </div>
//   </Router>
// );
// }
