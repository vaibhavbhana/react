import './App.css';
import Header from './MyComponents/Header';
import  {Todos}  from './MyComponents/Todos';
import Footer from './MyComponents/Footer';
import Addtodo from './MyComponents/Addtodo';
import React,{useState,useEffect} from 'react';


function App() {
  let initTodo;
  if(localStorage.getItem("todos")===null){
    initTodo=[];
  }
  else{
    initTodo=JSON.parse(localStorage.getItem("todos"));

  }
  const ondelete=(todo)=>{
    console.log("ondelete",todo)
    setTodos(todos.filter((e)=>{
      return e!==todo;
      
    }));
    localStorage.setItem("todos",JSON.stringify(todos));
 
   }


  const addTodo=(title, desc )=>{
    let sno
    if(todos.length===0){
      sno=0;
    }
    else{
       sno= todos[todos.length-1].sno +1 ;
    }
    const mytodo={
      title:title,
      desc:desc,
      sno:sno

    }
    setTodos([...todos,mytodo]);
    localStorage.setItem("todos",JSON.stringify(todos));
  

  }

 
  const [todos,setTodos ]= useState(initTodo);
  useEffect(() => {
    localStorage.setItem("todos",JSON.stringify(todos));
    
  }, [todos])
 

  return (
   <>
<Header title="My TodoList" searchBar={true}/>
<Addtodo addTodo={addTodo}/>
<Todos todos={todos} ondelete={ondelete} />
<Footer />

</>
  );
}

export default App;
