import React,{useState} from 'react';

export default function Addtodo(props) {
    const [title,setTitle]=useState("");
    const [desc,setDesc]=useState("");

    const submit=(e)=>{

        e.preventDefault();
        if(!title || !desc ){
        alert("title or descrpition is not empty");}
        props.addTodo(title,desc)


    }
    return (
        <div className="container my-3">
            <h3>Add a Todo</h3>

        <form  onSubmit={submit}>
            <div className="form-group mb-3">
                <label for="title"> Todo Title</label>
                <input type="text" className="form-control" value={title} id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Title" onChange={(e)=>{setTitle(e.target.value)}} />
             
            </div>
            <div className="form-group mb-3">
                <label for="desc"> Todo Description</label>
                <input type="text" className="form-control"value={desc} id="exampleInputPassword1" placeholder="Description" onChange={(e)=>{setDesc(e.target.value)}} />
            </div>
       
            <button type="submit" className="btn  btn-sm btn-success mb-3">Add Todo</button>
        </form>
        </div>

    )
}
