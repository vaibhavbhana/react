import React from 'react'
import  {Todoitem}  from '../MyComponents/Todoitem';



export const Todos = (props) => {
    console.log(props.todos) 
    return (
        <div className="container my-3"  style={{ minHeight:"54vh"}} >
            <h3 className=" my-4">Todos Lists</h3>
            { props.todos.length===0?"no todo to display":
            props.todos.map((todo)=>{
                return <Todoitem todo={todo}  key={todo.sno} ondelete={props.ondelete}/>
            })
            }
         {/* <Todoitem todo={props.todos}/> */}
           
            
            
        </div>
    )
}
