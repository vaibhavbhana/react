
import Navbar from './Navbar';
import Home from './home';
import { BrowserRouter as Router,Route,Switch } from 'react-router-dom';
import Create from './Create';
import BlogDetails from './BlogDetails';



function App() {
  // const title ="welcome to the  new blog";
  // const likes=50;
  
  // const link ="http://www.google.com"; 
  return (
    <Router>
      <div className="App">
        < Navbar />
      <div className="content"> 
      <Switch>
        <Route exact path="/">
        < Home/>
        </Route>
        <Route path="/create">
        < Create/>
        </Route>
        <Route  path="/blogs/:id">
        < BlogDetails />
        </Route>
      </Switch>
      
    
  
      
      {/* <a href={link}>google</a>
      <p>{Math.random(0,5)}</p> */}

      </div>
      </div>
    </Router>
  );
}

export default App;
