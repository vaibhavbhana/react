import { useState } from "react";
import { useHistory } from "react-router-dom";

const Create = () =>{

    const [title,setTitle]= useState('')
    const [body,setBody]= useState('')
    const [author,setAuthor]=useState('vaibav')
    const history = useHistory();
    const handleSubmit =(e)=>{
        e.preventDefault()
        const blog ={title,body,author};
        fetch(' http://localhost:8000/blogs',{
            method: 'POST',
            headers: {"content-type":"application/json"},
            body:JSON.stringify(blog)
        }).then(() =>{
            console.log("new blog  added")
            history.push('/')
        }
            
        );

    }
    return(
        <div className="create">
              <h2>Add a New Blog </h2>
              <form onSubmit={handleSubmit}>
                  <label>blog title :</label>
                  <input type="text"
                  required 
                  value={title}
                  onChange={(e)=> setTitle(e.target.value)}
                  />
                 
                  <label>blog body:</label>
                  <textarea 
                  required 
                  value={body}
                  onChange={(e)=> setBody(e.target.value)}
                  ></textarea>
                  <label>blog author :</label>
                  <select
                  value={author}
                  onChange={(e)=> setAuthor(e.target.value)}
                  >
                      <option value="vaibav">vaibav</option>
                      <option value="bhana">bhana</option>
                      <option value="yatika">yatika</option>
                  </select>
                  <button >Add Blog</button>
                 
                  
              </form>
           
        </div>
      
        
    );
}

export default Create;



