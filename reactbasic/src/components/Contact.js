import React from "react";

class Contact extends React.Component {

    
    state={
        name:"",
        email:""
    }
    

    add = (e)=> {
        e.preventDefault();
        if(this.state.name === "" && this.state.email === ""){
            alert(" All field are mandatory");
            return
        }
        console.log(this.state)
       this.props.ContactHandler(this.state);
       this.setState({name:"",email:""});
       this.props.history.push("/")
    }
    render (){
        return(
            <div className="ui main"><br/><br/>
                <h2> Add Contact</h2>
                <form className="ui form" onSubmit={this.add}>
                    <div className="field">
                        <label>Name</label>
                        
                        <input type="text" name="name" value={this.state.name} placeholder="name" onChange={(e)=> this.setState({name:e.target.value})} />
                    </div>
                    <div className="field">
                        <label>Email</label>
                       
                        <input type="text" name="email" value={this.state.email}  placeholder="email" onChange={(e)=> this.setState({email:e.target.value})} />
                    </div>o
                    <button className="ui button black" >Add</button>

                </form>
            </div>


        );
    }
}
export default Contact