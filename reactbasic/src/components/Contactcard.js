
import React from "react";
import { Link } from "react-router-dom";
const Contactcard=(props)=>{
    const {id,name,email}=props.contact
    return(
        <div className="item">
        <div className="content">
            <Link to ={{pathname:`/contact/${id}`,state:{contact:props.contact}}}>
            <div className="header">{name}</div>
            <div> {email}</div>
            </Link>
           
        </div><br />
        <i  style={{color:"red"}}   className="trash alternate outline icon" onClick={()=>{props.clickHander(id)}}></i>

    </div>

    )
}
export default Contactcard