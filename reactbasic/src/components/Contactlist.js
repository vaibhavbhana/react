import React from "react";
import { Link } from "react-router-dom";
import Contactcard from "./Contactcard";
const Contactlist =(props)=>{
    console.log(props)
    const deleteContactHandler=(id)=>{
        props.getContactid(id);
    };
    const renderContactlist = props.contacts.map((contact)=>{
        return(
          <Contactcard contact={contact} 
          clickHander={deleteContactHandler} 
          key={contact.id}/>
        )
    })
     return(
         <div  style ={{marginTop:"45px"}}className="main">
             <h2>Contact List
             <Link to="/add">  <button style={{marginLeft:"1600px"}}className="ui button blue right ">Add Contact</button> </Link></h2> 
                   

      <div className="ui celled list">{renderContactlist}</div>
      </div>
     )
}
export default Contactlist