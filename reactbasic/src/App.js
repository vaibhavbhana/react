import React, { useState ,useEffect}  from "react";
import Contact from "./components/Contact";
import { uuid } from "uuidv4";
import {BrowserRouter  as Router,Switch,Route} from 'react-router-dom';

import Contactlist from "./components/Contactlist";
import Header from "./components/Header";
import Contactdetail from "./components/Contactdetail";


function App() {
  const LOCAL_STORAGE_KEY ="contacts"
  const [contacts,Setcontact]=useState([]);
  const ContactHandler = (contact)=>{
    console.log(contact);
    Setcontact([...contacts,{id:uuid(), ...contact}])
  };
  const RemoveContactHandler = (id) =>{
    const newContactList=contacts.filter((contact)=>{return contact.id !== id;
    })
 Setcontact(newContactList);
  };
  useEffect(()=>{
    const retriveContacts = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if (retriveContacts) Setcontact(retriveContacts);

  },[]);
  useEffect(()=>{
    localStorage.setItem(LOCAL_STORAGE_KEY,JSON.stringify(contacts));
  },[contacts])


  return (
    <div className="App">
   
    <Router>
    <Header/>
      <Switch>
      <Route path= "/add"
      render ={(props)=>(<Contact{...props} ContactHandler={ContactHandler}/>)}
    />
  
    <Route  exact path="/">
    <Contactlist contacts={contacts} getContactid={RemoveContactHandler} /></Route>
    <Route path="/contact/:id" component={Contactdetail}/>
    
  
    </Switch>
    </Router>
    
    </div>
  );
}

export default App;
